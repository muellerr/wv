## Name
Suitability of game design elements in industrial production

## Description
In this interactive user interface, criteria for the use of gamification in industrial production can be selected. The system displays which elements are suitable for selection. It also shows how many of the selected categories an element fulfills. When an element is selected, an example and a short explanatory text are displayed.

## License
[MIT](https://choosealicense.com/licenses/mit/)
