// When selecting main criteria, select all criteria
function toggleCriteria() {
  var h1 = document.getElementById("h1");
  var checkBoxes = document.getElementsByClassName("h1");
  if (h1.checked == false) {
    for (let i = 0; i < checkBoxes.length; i++) {
      checkBoxes[i].checked = false;
    }
  } else {
    for (let i = 0; i < checkBoxes.length; i++) {
      checkBoxes[i].checked = true;
    }
  }

  var h2 = document.getElementById("h2");
  var checkBoxes = document.getElementsByClassName("h2");
  if (h2.checked == false) {
    for (let i = 0; i < checkBoxes.length; i++) {
      checkBoxes[i].checked = false;
    }
  } else {
    for (let i = 0; i < checkBoxes.length; i++) {
      checkBoxes[i].checked = true;
    }
  }

  var h3 = document.getElementById("h3");
  var checkBoxes = document.getElementsByClassName("h3");
  if (h3.checked == false) {
    for (let i = 0; i < checkBoxes.length; i++) {
      checkBoxes[i].checked = false;
    }
  } else {
    for (let i = 0; i < checkBoxes.length; i++) {
      checkBoxes[i].checked = true;
    }
  }

  var h4 = document.getElementById("h4");
  var checkBoxes = document.getElementsByClassName("h4");
  if (h4.checked == false) {
    for (let i = 0; i < checkBoxes.length; i++) {
      checkBoxes[i].checked = false;
    }
  } else {
    for (let i = 0; i < checkBoxes.length; i++) {
      checkBoxes[i].checked = true;
    }
  }

  getResults();
}

// Set maincriteria to true/false if all criterias are selected/one or more deselected
function criteriaOnChange1() {
  var checkBoxes = document.getElementsByClassName("h1");
  var checked = 0;

  for (let i = 0; i < checkBoxes.length; i++) {
    if (checkBoxes[i].checked) {
      checked++;
    }
  }

  if (checked == 2) {
    var h1 = document.getElementById("h1");
    h1.checked = true;
  } else {
    var h1 = document.getElementById("h1");
    h1.checked = false;
  }

  getResults();
}

function criteriaOnChange2() {
  var checkBoxes = document.getElementsByClassName("h2");
  var checked = 0;

  for (let i = 0; i < checkBoxes.length; i++) {
    if (checkBoxes[i].checked) {
      checked++;
    }
  }

  if (checked == 5) {
    var h2 = document.getElementById("h2");
    h2.checked = true;
  } else {
    var h2 = document.getElementById("h2");
    h2.checked = false;
  }
  getResults();
}

function criteriaOnChange3() {
  var checkBoxes = document.getElementsByClassName("h3");
  var checked = 0;

  for (let i = 0; i < checkBoxes.length; i++) {
    if (checkBoxes[i].checked) {
      checked++;
    }
  }

  if (checked == 3) {
    var h3 = document.getElementById("h3");
    h3.checked = true;
  } else {
    var h3 = document.getElementById("h3");
    h3.checked = false;
  }
  getResults();
}

function criteriaOnChange4() {
  var checkBoxes = document.getElementsByClassName("h4");
  var checked = 0;

  for (let i = 0; i < checkBoxes.length; i++) {
    if (checkBoxes[i].checked) {
      checked++;
    }
  }

  if (checked == 3) {
    var h4 = document.getElementById("h4");
    h4.checked = true;
  } else {
    var h4 = document.getElementById("h4");
    h4.checked = false;
  }
  getResults();
}

const numberCounter = {
  element1: 0,
  element2: 0,
  element3: 0,
  element4: 0,
  element5: 0,
  element6: 0,
  element7: 0,
  element8: 0
};

// Get the results of the elements
function getResults() {
  const criteria = {
    criteria11: ["element3"],
    criteria12: ["element3"],
    criteria21: ["element4"],
    criteria22: ["element1", "element2", "element5"],
    criteria23: ["element1"],
    criteria24: ["element1", "element2"],
    criteria25: ["element1", "element2"],
    criteria31: ["element2", "element3", "element4"],
    criteria32: ["element2", "element3", "element4"],
    criteria33: ["element6", "element7", "element8"],
    criteria41: ["element1", "element4", "element6"],
    criteria42: ["element1", "element2", "element4", "element6"],
    criteria43: ["element1", "element2", "element4", "element6"]
  };

  const elementsResult = {
    element1: 0,
    element2: 0,
    element3: 0,
    element4: 0,
    element5: 0,
    element6: 0,
    element7: 0,
    element8: 0
  };

  const elements = {
    element1: "Punkte",
    element2: "Abzeichen",
    element3: "Fortschrittsanzeige",
    element4: "Bestenlisten",
    element5: "Level",
    element6: "Avatar",
    element7: "Narrativ",
    element8: "Nicht-Spieler-Charaktere"
  };

  let elementCountArray = [];
  let elementNameArray = [];

  let countCheckedCriteria = 0;
  const criteriaElements = document.getElementsByClassName("criteria");

  for (let i = 0; i < criteriaElements.length; i++) {
    if (criteriaElements[i].checked) {
      countCheckedCriteria++;
      criteria[criteriaElements[i].id].forEach((item) => {
        elementsResult[item] = elementsResult[item] + 1;
      });
    }
  }

  let totalCount = 0;
  let percent = 0;

  console.log(elementsResult, "results");
  // Count totalCount up
  for (element in elementsResult) {
    if (elementsResult[element] > 0) totalCount++;

    elementCountArray.push(elementsResult[element]);
    elementNameArray.push(elements[element]);
  }

  //showElements(elementsResult, countCheckedCriteria)
  loopElements(elementsResult, countCheckedCriteria);

  console.log("array: ", elementCountArray);
  console.log("name: ", elementNameArray);

  generateChart(elementNameArray, elementCountArray);
}

function loopElements(elementsResult, countCheckedCriteria) {
  const circleElements = document.getElementsByTagName("circle");
  const numberElements = document.getElementsByClassName("number");
  console.log(circleElements);
  console.log(numberElements);
  console.log(elementsResult);

  let i = 0;
  for (var key in elementsResult) {
    console.log(elementsResult[key]);

    if (elementsResult[key] == 0) {
      resetCircleAndNumber(numberElements[i], circleElements[i], key);
    } else {
      modifyNumberAndText(
        circleElements[i],
        numberElements[i],
        elementsResult,
        elementsResult[key],
        countCheckedCriteria,
        key
      );
    }
    i++;
  }
}

// Shows elements
function modifyNumberAndText(
  circle,
  number,
  elementsResult,
  elementResultCount,
  countCheckedCriteria,
  key
) {
  let percent = 0;
  const elements = {
    element1: "Punkte",
    element2: "Abzeichen",
    element3: "Fortschrittsanzeige",
    element4: "Bestenlisten",
    element5: "Level",
    element6: "Avatar",
    element7: "Narrativ",
    element8: "Nicht-Spieler-Charaktere"
  };

  if (elementResultCount > 0) {
    // Get percent of each element
    if (countCheckedCriteria > 0) {
      percent = ((elementResultCount / countCheckedCriteria) * 100).toFixed(2);
    }

    // Increasing progess number
    let progress = 0;

    // Modify Number & Circle Progress
    modifiyCircleProgressNumber(number, percent, key);
    modifyCircleProgress(circle, percent);
  }
}

function modifyCircleProgress(circle, percent) {
  const progress = parseInt(235 - (235 * parseInt(percent)) / 100);
  circle.style.setProperty("--fill", progress);
}

function modifiyCircleProgressNumber(number, percent, key) {
  // Modify Number
  if (numberCounter[key] <= percent) {
    // Count up
    let intervalCountUp = setInterval(() => {
      if (numberCounter[key] >= percent) {
        clearInterval(intervalCountUp);
      } else {
        numberCounter[key] += 1;
        number.innerHTML = numberCounter[key] + "%";
      }
    }, 2000 / percent);
  } else {
    // Count down
    let intervalCountDown = setInterval(() => {
      if (numberCounter[key] <= percent) {
        clearInterval(intervalCountDown);
      } else {
        numberCounter[key] -= 1;
        number.innerHTML = numberCounter[key] + "%";
      }
    }, 2000 / percent);
  }
}

function resetCircleAndNumber(number, circle, key) {
  let intervalToZero = setInterval(() => {
    if (numberCounter[key] <= 0) {
      clearInterval(intervalToZero);
    } else {
      numberCounter[key] -= 1;
      number.innerHTML = numberCounter[key] + "%";
    }
  }, 2000 / numberCounter[key]);
  circle.style.setProperty("--fill", 234);
}

function setActive(e, svgId) {
  progressBarElements = document.getElementsByClassName("progressbar");
  svgElements = document.getElementsByClassName("svgContainer");
  svgToShow = document.getElementById(svgId);

  for (let i = 0; i < progressBarElements.length; i++) {
    progressBarElements[i].classList.remove("active");
  }

  for (let i = 0; i < svgElements.length; i++) {
    svgElements[i].style.display = "none";
  }

  svgToShow.style.display = "flex";
  e.classList.add("active");
}

// Chart JS
/*
The MIT License (MIT)

Copyright (c) 2014-2022 Chart.js Contributors

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

let chart;

function generateChart(elementNameArray, elementCountArray) {
  try {
    chart.destroy();
  } catch (error) {
    console.error(error);
    // Expected output: ReferenceError: nonExistentFunction is not defined
    // (Note: the exact output may be browser-dependent)
  }

  const ctx = document.getElementById("myChart");

  chart = new Chart(ctx, {
    type: "bar",
    data: {
      labels: elementNameArray,
      datasets: [
        {
          data: elementCountArray,
          borderWidth: 1,

          backgroundColor: "#92319c82"
        }
      ]
    },
    options: {
      responsive: true,
      maintainAspectRatio: false,

      plugins: {
        legend: {
          display: false
        }
      },
      scales: {
        y: {
          ticks: {
            stepSize: 1
          },
          beginAtZero: true,
          title: {
            display: true,
            text: "Anzahl Kriterien"
          }
        },
        x: {
          title: {
            display: true,
            text: "Spiel-Design-Elemente"
          }
        }
      }
    }
  });
}
